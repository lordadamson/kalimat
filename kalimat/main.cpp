/**************************************************************************
**   The Kalimat programming language
**   Copyright (C) 2010 Mohamed Samy Ali - samy2004@gmail.com
**   This project is released under the Apache License version 2.0
**   as described in the included license.txt file
**************************************************************************/

#include <QtGui/QApplication>

#include "../smallvm/vm_incl.h"
#include "../smallvm/vm.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#ifdef ENGLISH_PL
    QTranslator translator;
    translator.load(":/kalimat_en");
    a.installTranslator(&translator);
#endif
    MainWindow w;
    w.show();
    return a.exec();
}
